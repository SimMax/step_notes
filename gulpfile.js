'use strict';

const gulp = require('gulp');
const uglify = require('gulp-uglify-es').default;
const sass = require('gulp-sass');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const clean = require('gulp-clean');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const autoprefixer = require('gulp-autoprefixer');
const rename = require('gulp-rename');
const sourcemaps = require('gulp-sourcemaps');
const uglifycss = require('gulp-uglifycss');
const cssmin = require('gulp-cssmin');
const runSequence = require('run-sequence');

gulp.task('indexCopy', () => {
    gulp.src('./view/**/*.html')
        .pipe(gulp.dest('./public'));
});

gulp.task('img', () => {
    gulp.src('./src/images/**/*')
        .pipe(imagemin({
            interlaced: true,
            progressive: true,
            svgoPlugins: [{ removeViewBox: true }],
            use: [pngquant()]
        }))
        .pipe(gulp.dest('./public/images'));
});

gulp.task('clean', () => {
    return gulp.src('public', { read: false })
        .pipe(clean());
});

gulp.task('sass', () => {
    gulp.src('./src/scss/**/*.scss')
        .pipe(autoprefixer({
            overrideBrowserslist: ['last 2 versions'],
            cascade: false
        }))
        .pipe(sourcemaps.init())
        .pipe(sass())
        .pipe(cssmin())
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(gulp.dest('./public/css'));
    gulp.src('./src/scss/reset.css')
        .pipe(rename({ suffix: '.min' }))
        .pipe(uglifycss({
            "maxLineLen": 80,
            "uglyComments": true
        }))
        .pipe(gulp.dest('./public/css'));
});

gulp.task('dev', ['sass', 'uglify', 'img'], () => {
    browserSync.init({
        server: '/public'
    });
    gulp.watch('./src/js/**/*.js', ['uglify']).on('change', browserSync.reload);
    gulp.watch('./src/scss/**/*.scss', ['sass']).on('change', browserSync.reload);
    gulp.watch('./view/**/*.html', ['indexCopy']).on('change', browserSync.reload);
});

gulp.task('build', (callback) => {
    runSequence('clean', ['sass', 'uglify', 'img', 'create-fonts-files', 'copy-external-modules', 'indexCopy'], callback);
});

gulp.task('copy-external-modules', () => {
    gulp.src('./src/modules/**/*')
        .pipe(gulp.dest('./public/modules'));
});

gulp.task('create-fonts-files', () => {
    gulp.src('./src/fonts/**/*')
        .pipe(gulp.dest('./public/fonts'));
});

gulp.task('uglify', () => {
    gulp.src(['./src/js/*.js', '!./src/js/*.test.js'])
        .pipe(concat('bundle.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./public/js'));
});

gulp.task('default', ['dev']);
