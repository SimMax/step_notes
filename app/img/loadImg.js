const multer = require('multer');
const crypto = require('crypto');
const path = require('path');

module.exports = (app) => {
    let fileName = '';
    let storage = multer.diskStorage({
        destination: function (req, file, callback) {
            callback(null, './public/images/');
        },
        filename: function (req, file, callback) {
            crypto.pseudoRandomBytes(16, function (err, raw) {
                if (err) return callback(err);
                fileName = (raw.toString('hex') + path.extname(file.originalname));
                callback(null, raw.toString('hex') + path.extname(file.originalname))
            })
        }
    });

    let upload = multer({ storage: storage }).single('noteImg');

    app.post('/load', function (req, res) {
        upload(req, res, function (err) {
            if (err) {
                return res.send("Error uploading file.");
            }
            res.send(fileName);
        });
    });
};
