module.exports = (app, db) => {
    app.get('/show', async (req, res) => {
        let query = {};
        let result = {};
        try {
            result = await db.collection('Notes').find(query).toArray();
        } catch(err) {
            console.log(err);
        }
        res.send(JSON.stringify(result));
    });
};