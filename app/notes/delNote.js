const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });
const fs = require('fs');

module.exports = (app, db) => {
    app.delete('/del', urlencodedParser, async (req, res) => {
        let query = { _id: req.body.id };
        try {
            await db.collection('Notes').deleteOne(query);
            if(req.body.imgName !== 'none'){
                fs.unlinkSync(`./public/images/${req.body.imgName}`);
            }
            res.send(JSON.stringify(req.body));
        } catch (err) {
            console.log(err);
        }
    });
};