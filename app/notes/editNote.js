const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = (app, db) => {
    app.put('/edit', urlencodedParser, async (req, res) => {
        let query = { _id: req.body._id };
        let dataForUpd = { 
            type: req.body.type,
            title: req.body.title, 
            data: req.body.data, 
            date: req.body.date,
            bgColor: req.body.bgColor,
            img: req.body.img
        };

        try {
            await db.collection('Notes').updateOne( query, { $set: dataForUpd }, { upsert: true });
            res.send(JSON.stringify(req.body));
        } catch (err) {
            console.log(err);
        }
    });
}
