const bodyParser = require('body-parser');
const urlencodedParser = bodyParser.urlencoded({ extended: false });

module.exports = (app, db) => {
    app.post('/add', urlencodedParser, (req, res) => {
        db.collection('Notes').insertOne(req.body, (err, result) => {
            if (err) console.log(err);
            console.log("id note:" + result.insertedId); 
            res.send(JSON.stringify(result.ops[0]));
        });
    });
};