'use strict';

function showNote(elem) {
    let dataContainer = $('<div/>', {
        class: 'card-content'
    });

    if (elem.type === 'note') {
        $('<p/>', {
            class: 'card-text',
            text: elem.data
        }).appendTo(dataContainer);

        if (elem.img !== 'none') {
            $('<img/>', {
                class: 'card-img',
                'src': `./images/${elem.img}`,
                'alt': 'Card image cap'
            })
            .prependTo(dataContainer);
        }
    } else if (elem.type === 'list') {
        let list = $('<ul/>', {
            class: 'list list-unstyled'
        });
        let activeTasksCounter = elem.data.length;

        elem.data.forEach((task) => {
            let li = $('<li/>', {
                class: 'task ui-state-default',
                text: task.text
            });
            if (task.checked === true) {
                activeTasksCounter--;
                li.addClass('text-line-through');
            };
            li.appendTo($(list));
        });

        list.appendTo(dataContainer);

        $('<p/>', {
            text: ' active tasks',
            prepend: $('<strong/>', {
                text: activeTasksCounter
            })
        }).insertAfter(list);
    }

    $('<small/>', {
        class: 'edit-date text-muted',
        text: `Last updated: ${elem.date}`
    }).appendTo(dataContainer);

    let card = $('<div/>', {
        class: `card ${elem.bgColor}`,
        'data-id': elem._id,
        'data-type': elem.type,
        'data-toggle': 'modal',
        append: ($('<div/>', {
            class: 'card-body',
            append: $('<h5/>', {
                class: 'card-title',
                text: elem.title
            })
                .add(dataContainer)
        })
        )
    });

    if (elem.type === 'note') {
        card.attr('data-target', '#modal_for_note');
        card.on('click', (event) => {
            let modal =  $('#modal_for_note');
            modal.data('state', 'existing');
            
            $('.modal-content').toggleClass('white green blue red', false);
            let hasClass = $(event.target).parents('.card').attr('class').split(' ');
            $('.modal-content').addClass(`${hasClass[hasClass.length - 1]}`);
           
            modal.find('.add-data').addClass('d-none'); 
            modal.find('.show-data').removeClass('d-none'); 
            modal.find('.modal-title').text(elem.title); 
            
            modal.find('.show-data').html($(event.target).parents('.card').find('.card-content').html());

            modal.find('.btn-edit').removeClass('d-none');
            modal.find('.btn-delete').removeClass('d-none');
            modal.find('.btn-save').addClass('d-none');

            modal.data('id', elem._id);
            modal.data('type', elem.type);
        });

    } else if (elem.type === 'list') {
        card.attr('data-target', '#modal_for_list');
        card.on('click', (event) => {
            let modal =  $('#modal_for_list');
            modal.data('state', 'existing');
            modal.find($('.modal-title')).text(elem.title);
            modal.data('id', elem._id);
            modal.data('type', elem.type);

            elem.data.forEach((task) => {
                createTask(task.text, modal, task.id, task.checked);
            });

            list_items_render(modal.find('.list'), true);
            list_items_render(modal.find('.list'), false);

            modal.data('itemsCount', elem.itemsCount); 
            modal.find($('#new_list_title')).val(elem.title).parent('.form-group').addClass('d-none');
            modal.find($('#add_todo')).addClass('d-none');
            modal.find($('.remove-item')).addClass('d-none');
            modal.find($('#checkAll')).removeClass('d-none');
            modal.find($('.btn-edit')).removeClass('d-none');
            modal.find($('.btn-delete')).addClass('d-none');
        });
    }
    return card;
}