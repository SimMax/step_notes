const chai = require('chai');
const chaiAsPromised = require('chai-as-promised');
const assert = chai.assert;
const expect = chai.expect;
const should = chai.should;

chai.use(chaiAsPromised);

describe('Поиск повторяющихся id', () => {
    let arr = [];
    let count = 0;

    function generateId() {
        let randomFirst = Math.floor(Math.random() * (1000000 + 1));
        let randomSecond = Math.floor(Math.random() * (1000000 + 1));
        return Number(randomFirst).toString(16) + Number(randomSecond).toString(16);
    }

    for (let i = 0; i < 1000; i++) {
        arr.push(generateId());
    }

    for(let i = 0; i < arr.length; i++){
        for(let j = arr.length - 1; j > i; j--){
            if(arr[i] === arr[j]){
                console.log(i+ ':' + arr[i] + '=' + j + ':' + arr[j]);
                count++;
            }
            it(`${i}:${arr[i]} != ${j}:${arr[j]}`, function() {
                assert.notEqual(arr[i], arr[j]);
            });
        }
    }
    console.log('Количество повторений: ' + count);
});
