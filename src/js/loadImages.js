'use strict';

function getFileName() {
    let file = $('#uploaded-file').val();
    file = file.replace(/\\/g, '/').split('/').pop();
    $('#file-name').html('Image name: ' + file);
    $('.file-upload_submit').prop('disabled', false);
    $('.check').hide();
    $('.cross').hide();
}

$(document).ready(function () {
    let image = $('<img/>', {
        class: 'card-img mb-1 mt-1',
        'alt': 'Card image cap'
    });

    $('#uploadImg').on('submit', function (e) {
        e.preventDefault();
        $('.add-data .card-img').remove();
        $('.preloader').show();
        $('.check').hide();
        $('.cross').hide();
        let formData = new FormData($(this).get(0)); 
        
        $.ajax({
            type: 'POST',
            url: 'http://localhost:3000/load',
            contentType: false, 
            processData: false, 
            data: formData,
            success: function (fileName) {
                image.attr('src', `./images/${fileName}`);
                $('#modal_for_note').find('.add-img').append(image);
                $('#uploadImg').data('img', fileName);
                $('.preloader').hide();
                $('.check').show();
                $('.file-upload_submit').prop('disabled', true);
            },
            error: function (err) {
                $('.preloader').hide();
                $('.cross').show();
            }
        });
        return false;
    });
});

