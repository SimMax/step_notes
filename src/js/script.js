'use strict';

function NewNote(id, type, title, data, date, bgColor = '#ffffff', img = 'none', itemsCount = 0) {
    this._id = id;
    this.img = img;
    this.type = type;
    this.title = title;
    this.data = data;
    this.date = date;
    this.bgColor = bgColor;
    this.itemsCount = itemsCount;
}

(() => {
    $.ajax({
        type: 'GET',
        url: 'http://localhost:3000/show',
        dataType: 'json',
        contentType: 'application/json',
        success: (cards) => {
            cards.forEach(elem => {
                $('.card-columns').prepend(showNote(elem));
            });
        },
        error: (err) => {
            dangerAlert();
            console.log(err);
        }
    });
})();

$('#btn_new_note').on('click', () => {
    $('#modal_for_note').data('state', 'new');
    $('.modal-content').toggleClass('white green blue red', false);
    $('#modal_for_note input[value="white"]:radio').prop('checked', true);
});

$('#modal_for_note').on('show.bs.modal', (event) => {
    if ($('#modal_for_note').data('state') === 'new') {
        let modal = $(event.target),
            noteID = generateId();
        modal.data('id', noteID);
        modal.data('type', 'note');

        modal.find('.modal-title').text('New note');
        modal.find('.add-data').removeClass('d-none'); 
        modal.find('.show-data').addClass('d-none'); 

        modal.find($('.btn-save')).removeClass('d-none');
        modal.find($('.btn-edit')).addClass('d-none');
        modal.find($('.btn-delete')).addClass('d-none');
    }
});

$('#modal_for_note').on('hidden.bs.modal', (event) => {
    let modal = $(event.target);
    modal.data('id', '');
    modal.data('type', '');

    modal.find('.note_title').val('');
    modal.find('.note_body').val('');

    modal.find('#uploadImg').data('img', 'none');
    modal.find('.card-img').remove();
    modal.find('.check').hide();
    modal.find('.cross').hide();
    modal.find('#file-name').text('');
    modal.find('.file-upload_submit').prop('disabled', true);
});

$('.btn-save').on('click', function (event) {
    let modal = $(this).parents('.modal'),
        noteID = modal.data('id'),
        noteType = modal.data('type'),
        noteTitle = modal.find('.note_title').val(),
        noteData = [],
        noteDate = new Date().toLocaleString(),
        noteBgColor = modal.find('input[name="colorSet"]:checked').val(),
        noteImg = modal.find('#uploadImg').data('img'),
        itemsCount = 0,
        new_note = {},
        ajaxRequestType = 'POST',
        ajaxRequestRoute = 'add';

    if (noteType === 'list') {
        itemsCount = modal.data('itemsCount');
        $.each(modal.find('.list-item__text'), (index, value) => {
            let temp_obj = {
                id: $(value).prop('for'),
                text: $(value).text(),
                checked: $(value).siblings('input:checkbox').prop('checked')
            };
            noteData.push(temp_obj);
        });
    } else if (noteType === 'note') {
        noteData = $('#body_new_note').val();
    }

    if (modal.data('state') === 'existing') {
        ajaxRequestType = 'PUT';
        ajaxRequestRoute = 'edit';
    }

    new_note = new NewNote(noteID, noteType, noteTitle, noteData, noteDate, noteBgColor, noteImg, itemsCount);

    $.ajax({
        type: ajaxRequestType,
        url: `http://localhost:3000/${ajaxRequestRoute}`,
        data: JSON.stringify(new_note),
        dataType: 'json',
        contentType: 'application/json',
        success: (res) => {
            successAlert();
            if (res.type === 'note') {
                modal.find('.note_title').val('');
                modal.find('.note_body').val('');
                modal.find('#uploadImg').data('img', 'none');
                modal.find('input[name="noteImg"]').val('');
                modal.find('.card-img').remove();

                modal.find('#modal_for_note input[value="white"]:radio').prop('checked', true);
                modal.find('.preloader').hide();
                modal.find('.check').hide();
                modal.find('.cross').hide();
                modal.find('#file-name').text('');

            } else if (res.type === 'list') {
                modal.find('.note_title').val('');
                modal.find('.add-todo').val('');
                modal.find('.list').text('');
            }

            if (ajaxRequestType === 'POST') {
                $('.card-columns').prepend(showNote(res))
            } else if (ajaxRequestType === 'PUT') {
                $(`.card[data-id="${res._id}"]`).replaceWith(showNote(res));
            }
            modal.modal('hide');
        },
        error: (err) => {
            dangerAlert();
            console.log(err);
        }
    });
});

$('.btn-delete').on('click', function (event) {
    let imgSrc = $(this).parents('.modal').find('.show-data .card-img').attr('src');
    let imgName = '';

    if(imgSrc !== undefined){
        imgSrc = imgSrc.split('/');
        imgName = imgSrc[imgSrc.length-1]
    } else {
        imgName = 'none';
    }

    let delItem = { 
        'id': `${$(event.target).parents('.modal').data('id')}`,
        'imgName': `${imgName}`
    };

    $.ajax({
        type: 'DELETE',
        url: 'http://localhost:3000/del',
        data: JSON.stringify(delItem),
        dataType: 'json',
        contentType: 'application/json',
        success: (res) => {
            successAlert();
            $(event.target).parents('.modal').modal('hide');

            $(`.card-columns [data-id='${res.id}']`).remove();
        },
        error: (err) => {
            dangerAlert();
            console.log(err);
        }
    });
});

$('.btn-edit').on('click', (event) => {
    let btn = $(event.target),
        modal = btn.parents('.modal');

    if (modal.attr('id') === 'modal_for_note') {
        let cardsColor = modal.find($('.modal-content')).attr('class').split(' ');
        $(`#modal_for_note input[value="${cardsColor[cardsColor.length - 1]}"]:radio`).prop('checked', true);
        $('.modal-content').toggleClass('white green blue red', false);

        modal.find('.add-data').removeClass('d-none'); 
        modal.find('.show-data').addClass('d-none'); 
       
        modal.find('.note_title').val(modal.find('.modal-title').text());
        modal.find('.note_body').val(modal.find('.card-text').text());

        if(modal.find('.show-data .card-img').attr('src') !== undefined){
            let imgName = modal.find('.show-data .card-img').attr('src').split('/');
            modal.find('#uploadImg').data('img', `${imgName[imgName.length - 1]}`);
     
            let image = $('<img/>', {
                class: 'card-img mb-1 mt-1',
                'alt': 'Card image cap'
            });
            image.attr('src', `${modal.find('.show-data .card-img').attr('src')}`);
            modal.find('.add-img').append(image);
        }

        modal.find('.btn-save').removeClass('d-none');
        modal.find('.btn-edit').addClass('d-none');
        modal.find('.btn-delete').addClass('d-none');

        modal.find('.modal-title').text('Editing');

    } else if (modal.attr('id') === 'modal_for_list') {
        modal.find('.note_title').parent('.form-group').removeClass('d-none');
        modal.find('#add_todo').removeClass('d-none');
        modal.find('.remove-item').removeClass('d-none');
        modal.find('#checkAll').addClass('d-none');
        btn.addClass('d-none');
        modal.find($('.btn-delete')).removeClass('d-none');
    }
});

function successAlert(){
    $('#success-alert').fadeTo(2000, 500).slideUp(500, () => {
        $('#success-alert').slideUp(500);
    });
}

function dangerAlert(){
    $('#danger-alert').fadeTo(2000, 500).slideUp(500, () => {
        $('#danger-alert').slideUp(500);
    });
}

function generateId() {
    let randomFirst = Math.floor(Math.random() * (1000000 + 1));
    let randomSecond = Math.floor(Math.random() * (1000000 + 1));
    return Number(randomFirst).toString(16) + Number(randomSecond).toString(16);
}

$('.note_title').on('keypress', function (event) {
    event.preventDefault;
    if (event.which === 13) {
        if ($(this).val() !== '') {
            $('.note_body').focus();
        } 
    }
});