'use strict';

$('#btn_new_list').on('click', () => {
    $('#modal_for_list').data('state', 'new');
});

$('#modal_for_list').on('show.bs.modal', (event) => {
    if ($('#modal_for_list').data('state') === 'new') {
        let modal = $(event.target),
            listID = generateId(),
            count = countTasks(modal);

        modal.find($('.modal-title')).text('New list');
        modal.find('.count-todos').text(count);

        modal.find($('#new_list_title')).parent('.form-group').removeClass('d-none');
        modal.find($('#add_todo')).removeClass('d-none');
        modal.find($('#checkAll')).addClass('d-none');
        modal.find($('.btn-edit')).addClass('d-none');
        modal.find($('.btn-delete')).addClass('d-none');

        modal.data('id', listID);
        modal.data('type', 'list');
        modal.data('itemsCount', count + 1); 
    }
});

$('#modal_for_list').on('hidden.bs.modal', (event) => {
    let modal = $(event.target);

    modal.data('id', '');
    modal.data('type', '');
    modal.data('itemsCount', 0);

    modal.find('.modal-title').text('');
    modal.find('.note_title').val('');
    modal.find('#add_todo').val('');
    modal.find('.list').text('');
    modal.find('.count-todos').text(modal.data('itemsCount'));
});

//add list title
$('#new_list_title').on('keypress', function (event) {
    event.preventDefault;
    if (event.which === 13) {
        if ($(this).val() !== '') {
            $('#add_todo').focus();
        } else {
            // some validation
        }
    }
});

//create todo
$('.add-todo').on('keypress', function (event) {
    event.preventDefault;
    if (event.which === 13) {
        let modal = $(event.target).parents('.modal');

        if ($(this).val() != '') {
            console.log($(modal).data('itemsCount'));
            let task = $(this).val(),
                id = (parseInt($(modal).data('id'), 16) + $(modal).data('itemsCount') + 1).toString(16);

            createTask(task, $(modal), id, false);
            $(modal).data('itemsCount', $(modal).data('itemsCount') + 1);
            console.log($(modal).data('itemsCount'))

        } else {
            // some validation
        }
    }
});

// all done btn
$('#checkAll').click(function () {
    $(this).parents('.modal').find('.custom-control-input:not(:checked)').prop('checked', true);
    list_items_render($(this).parents('.modal').find('.list'), false);
});

// count tasks
function countTasks(current) {
    let count = current.find($('.list li')).length;
    return count;
}

//create task
function createTask(text, parent, id, state = false) {

    let li = $('<li/>', {
        class: 'list_item ui-state-default',
        append: $('<div/>', {
            class: 'custom-checkbox custom-control d-flex justify-content-between align-items-center', //toggle .custom-control to remove padding-left
            append: $('<input>', {
                id: id,
                class: 'custom-control-input',
                type: 'checkbox',
                change: function (e) {
                    let movedItem = $(this).parents('.ui-state-default');

                    if ($(this).prop('checked')) {
                        console.log('Checked');

                        list_items_render($(movedItem).parent('.list'), false);
                    } else {
                        console.log('Unchecked');

                        list_items_render($(movedItem).parent('.list'), true);
                    }
                },
                checked: state,
                disabled: true //remove later
            })
                .add($('<label/>', {
                    class: 'custom-control-label list-item__text', //toggle class .custom-control-label to show checkbox
                    for: id,
                    append: $('<p/>', {
                        text: text
                    })
                })
                    .add($('<button>', {
                        class: 'remove-item btn btn-danger btn-sm pull-right shadow',
                        append: $('<i/>', {
                            class: 'fas fa-times'
                        }),
                        click: (e) => {
                            $(e.target).parents('li').remove();
                            parent.find($('.count-todos')).html(countTasks(parent));
                        }
                    })))
        })
    });

    if (parent.find('.custom-control-input:checked').length > 0) {
        li.insertBefore(parent.find('.custom-control-input:checked:first').parents('li'));

    } else {
        li.appendTo(parent.find('.list'));
    }

    if (parent.data('state') === 'existing') {
        parent.find('.custom-checkbox').addClass('custom-control');
        parent.find('.custom-control-input').attr('disabled', false);
        parent.find('.list-item__text').addClass('custom-control-label');
    }

    parent.find($('.count-todos')).html(countTasks(parent));

    $('.add-todo').val('');
}

function list_items_render(container, state) {
    let temp = [];

    if (state === true) {
        $.each(container.find('input:checkbox:not(:checked)'), (key, value) => {
            temp.push($(value).parents('li'));
        });
        temp.sort((a, b) => {
            let firstID = $(a).find('input').attr('id'),
                secondID = $(b).find('input').attr('id');
            return parseInt(secondID, 16) - parseInt(firstID, 16);
        });

        temp.forEach((elem) => {
            container.prepend($(elem));
        });
    } else {
        $.each(container.find('input:checkbox:checked'), (key, value) => {
            temp.push($(value).parents('li'));
        });

        temp.sort((a, b) => {
            let firstID = $(a).find('input').attr('id'),
                secondID = $(b).find('input').attr('id');
            return parseInt(firstID, 16) - parseInt(secondID, 16);
        });

        temp.forEach((elem) => {
            container.append($(elem));
        });
    }
}