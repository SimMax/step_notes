const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const rfs = require('rotating-file-stream');
const MongoClient = require('mongodb').MongoClient;
const db = require('./config/db.js');
const app = express();

let logDirectory = path.join(__dirname, 'log');
let accessLogStream = rfs('access.log', {
    interval: '1d',
    path: logDirectory
});

app.use(cors());
app.use(bodyParser.json());
app.use(morgan('combined', { stream: accessLogStream }));
app.use(express.static('public'));

app.route('/')
    .get((req, res) => {
        res.sendFile(__dirname + "/view/index.html");
    });

MongoClient.connect(db.url, { useNewUrlParser: true }, (err, client) => {
    if (err) return console.log(err);
    const database = client.db(db.dbName);
    require('./routes')(app, database);
    app.listen(3000, () => {
        console.log('Connected to ' + db.url);
    });
});