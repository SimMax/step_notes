const addNote = require('./app/notes/addNote');
const delNote = require('./app/notes/delNote');
const editNote = require('./app/notes/editNote');
const showNotes = require('./app/notes/showNotes');
const loadImg = require('./app/img/loadImg');

module.exports = (app, db) => {
    addNote(app, db);
    delNote(app, db);
    editNote(app, db);
    showNotes(app, db);
    loadImg(app);
};